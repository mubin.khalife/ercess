from django.shortcuts import render, HttpResponseRedirect, redirect
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.contrib.auth import login as auth_login
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings
import threading


class LandingPage(generic.TemplateView):
    template_name = "index.html"


class Dashboard(LoginRequiredMixin, generic.TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context


class EmailThread(threading.Thread):
    def __init__(self, subject, html_content, recipient_list):
        self.subject = subject
        self.recipient_list = recipient_list
        self.html_content = html_content
        threading.Thread.__init__(self)

    def run(self):
        email_from = settings.EMAIL_HOST_USER
        send_mail(self.subject, self.html_content,
                  email_from, self.recipient_list)


class LoginPage(generic.TemplateView):
    model = User
    template_name = "login.html"

    def get_context_data(self, **kwargs):
        context = super(LoginPage, self).get_context_data(**kwargs)
        context['login_form'] = LoginForm
        context['signup_form'] = SignupForm
        return context

    def post(self, request, **kwargs):
        if request.POST.get('submit') == 'sign_in':
            form = LoginForm(request.POST)
            user = form.authenticate(username=request.POST.get(
                'username'), password=request.POST.get('password'),
                request=request)

            if user is not None and user != "err_pwd":
                # print("make user logged in ")
                auth_login(request, user)
                return HttpResponseRedirect('/dashboard')
            elif user == "err_pwd":
                messages.error(request, "Invalid credentials")
                return render(request, 'login.html', {'login_form':
                                                      LoginForm,
                                                      'signup_form':
                                                      SignupForm})
            else:
                # print("raise error")
                messages.error(request, "User not found in the system")
                return render(request, 'login.html', {'login_form':
                                                      form,
                                                      'signup_form':
                                                      SignupForm})
        elif request.POST.get('submit') == 'sign_up':
            # print("sign up posted")
            form = SignupForm(request.POST)
            if form.is_valid():
                msgValidation = form.validateValues()
                if msgValidation is not None and len(msgValidation) > 0:
                    messages.error(request, msgValidation)
                    return render(request, 'login.html', {'login_form':
                                                          LoginForm,
                                                          'signup_form':
                                                          form})
                else:
                    user = form.save(commit=False)
                    user.active = True
                    user.staff = False
                    user.admin = False
                    user.set_password(request.POST.get('password'))
                    user.save()

                    message = 'Hi {}, we have received your details and \
                        will process soon.'.format(
                        request.POST.get('username',))
                    # recipient_list = [request.POST.get('email',), ]
                    recipient_list = [request.POST.get(
                        'email', 'vishaljaiswal.info@gmail.com'), ]
                    EmailThread("Thank you to registration",
                                message, recipient_list).start()
                    auth_login(
                        request, user, backend='django.contrib.auth.backends.ModelBackend')
                    return redirect('/dashboard')

            else:
                print("Invalid signup form")
                
                messages.error(request, form.errors["__all__"])
                form.errors['__all__'] = None
                return render(request, 'login.html', {'login_form':
                                                      LoginForm,
                                                      'signup_form':
                                                      form})
        return JsonResponse(request.POST)
