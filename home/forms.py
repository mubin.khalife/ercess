from django import forms
from django.contrib.auth.models import User
import re
from crispy_forms.helper import FormHelper


class LoginForm(forms.ModelForm):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("username", "password")

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "Name"

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            user = User._default_manager.get_by_natural_key(username)
            if user.check_password(password):
                return user
            else:
                return "err_pwd"
                # User().set_password(password)
                # return user
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            return None


class SignupForm(forms.ModelForm):
    # name = forms.CharField(required=True,)
    email = forms.EmailField(required=True,)
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("username", "email", "password", "confirm_password")

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "Name"
        self.fields['username'].help_text = None
        self.helper = FormHelper(self)
        
    def clearFormError(self):
        self.helper.form_show_errors = False

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        name = cleaned_data.get("name")
        email = cleaned_data.get("email")
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        # existing_name = User.objects.filter(email__iexact=name)

        # existing_email = User.objects.filter(username__iexact=email)
        existing_name = User.objects.filter(username=name)

        existing_email = User.objects.filter(email=email)

        if existing_name.exists():
            raise forms.ValidationError(
                ("A user with this name already exists."))

        if existing_email.exists():
            raise forms.ValidationError(
                ("A user with this e-mail already exists."))

        regex = re.compile('[@_!#$%^&*()<>?/\|}{~:]')

        if len(password) < 8:
            raise forms.ValidationError(
                "Password should contain atleast 8 characters"
            )
        elif password != confirm_password:
            raise forms.ValidationError(
                "Password and Confirm password does not match"
            )
        elif(regex.search(password) is None):
            raise forms.ValidationError(
                "Password should contain atleast one special character"
            )

    def validateValues(self):
        cleaned_data = super(SignupForm, self).clean()
        name = cleaned_data.get("name")
        email = cleaned_data.get("email")
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        existing_name = User.objects.filter(username=name)

        existing_email = User.objects.filter(email=email)

        if existing_name.exists():
            return "A user with this name already exists."
            # return forms.ValidationError(
            #     ("A user with this name already exists."))

        if existing_email.exists():
            return "A user with this e-mail already exists."
            # return forms.ValidationError(
            #     ("A user with this e-mail already exists."))

        regex = re.compile('[@_!#$%^&*()<>?/\|}{~:]')

        if len(password) < 8:
            return"Password should contain atleast 8 characters"
        elif password != confirm_password:
            return "Password and Confirm password does not match"
        elif(regex.search(password) is None):
            return "Password should contain atleast one special character"
