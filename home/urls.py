from django.urls import path
from .views import LandingPage, Dashboard, LoginPage
from django.contrib.auth import views as auth_views

app_name = "home"

urlpatterns = [
    path('', LandingPage.as_view(), name="index"),
    path('login/', LoginPage.as_view(), name="login"),
    path('logout/', auth_views.LogoutView.as_view(
        template_name="index.html"), name="logout"),
    path('dashboard/', Dashboard.as_view(), name="dashboard"),
]
